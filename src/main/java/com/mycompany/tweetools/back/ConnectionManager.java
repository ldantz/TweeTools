package com.mycompany.tweetools.back;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;


public class ConnectionManager {

    /**
     * The database url
     */
    private static final String url = "jdbc:mysql://mysql-tweetools.alwaysdata.net/tweetools_database?allowMultiQueries=true";

    /**
     * JDBC driver
     */
    private static final String driver = "com.mysql.cj.jdbc.Driver";

    /**
     * The username (login) to access the database
     */
    private static final String user = "tweetools";

    /**
     * The password to access the database
     */
    private static final String password = "root";


    /**
     * Return an available connection from the pool
     *
     * @return an available connection
     */
    public static Connection getConnection() {
        Connection co = null;
      // boolean test = isOnline();

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {
            co = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return co;

    }


    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }




    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }

}

